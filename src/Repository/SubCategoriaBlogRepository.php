<?php

namespace App\Repository;

use App\Entity\SubCategoriaBlog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SubCategoriaBlog|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubCategoriaBlog|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubCategoriaBlog[]    findAll()
 * @method SubCategoriaBlog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubCategoriaBlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubCategoriaBlog::class);
    }

    // /**
    //  * @return SubCategoriaBlog[] Returns an array of SubCategoriaBlog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubCategoriaBlog
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
