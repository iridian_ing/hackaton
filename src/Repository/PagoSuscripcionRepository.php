<?php

namespace App\Repository;

use App\Entity\PagoSuscripcion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PagoSuscripcion|null find($id, $lockMode = null, $lockVersion = null)
 * @method PagoSuscripcion|null findOneBy(array $criteria, array $orderBy = null)
 * @method PagoSuscripcion[]    findAll()
 * @method PagoSuscripcion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagoSuscripcionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PagoSuscripcion::class);
    }

    // /**
    //  * @return PagoSuscripcion[] Returns an array of PagoSuscripcion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PagoSuscripcion
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
