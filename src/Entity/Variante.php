<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VarianteRepository")
 */
class Variante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoVariante")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tipo;

     /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Producto", mappedBy="variantes")
     */
    private $productos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $valor_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $valor_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $valor_fr;

    public function __construct()
    {
        $this->productos = new ArrayCollection();
    }


    public function __toString()
    {
        return $this->getTipo().' - '.$this->getValor();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?TipoVariante
    {
        return $this->tipo;
    }

    public function setTipo(?TipoVariante $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getValor(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'valor_'.$locale;
        return $this->{$field};
    }

    public function setValor(string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->addVariante($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            $producto->removeVariante($this);
        }

        return $this;
    }

    public function getValorEs(): ?string
    {
        return $this->valor_es;
    }

    public function setValorEs(?string $valor_es): self
    {
        $this->valor_es = $valor_es;

        return $this;
    }

    public function getValorEn(): ?string
    {
        return $this->valor_en;
    }

    public function setValorEn(?string $valor_en): self
    {
        $this->valor_en = $valor_en;

        return $this;
    }

    public function getValorFr(): ?string
    {
        return $this->valor_fr;
    }

    public function setValorFr(?string $valor_fr): self
    {
        $this->valor_fr = $valor_fr;

        return $this;
    }
}
