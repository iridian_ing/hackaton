<?php

namespace App\Repository;

use App\Entity\CategoriaVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CategoriaVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriaVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriaVideo[]    findAll()
 * @method CategoriaVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoriaVideo::class);
    }

    // /**
    //  * @return CategoriaVideo[] Returns an array of CategoriaVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoriaVideo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
