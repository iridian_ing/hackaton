<?php

namespace App\Repository;

use App\Entity\TipoTransferencia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoTransferencia|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoTransferencia|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoTransferencia[]    findAll()
 * @method TipoTransferencia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoTransferenciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoTransferencia::class);
    }

    // /**
    //  * @return TipoTransferencia[] Returns an array of TipoTransferencia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoTransferencia
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
