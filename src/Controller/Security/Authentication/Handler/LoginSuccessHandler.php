<?php
/**
 * Created by PhpStorm.
 * User: Iridian 1
 * Date: 2/03/2016
 * Time: 4:31 PM
 */

namespace App\Controller\Security\Authentication\Handler;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected
        $router,
        $security;

        public function __construct(Router $router, AuthorizationCheckerInterface $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $referer_url = $request->headers->get('referer');
        if($this->security->isGranted('ROLE_ADMIN')) {
            $referer_url  = 'admin_wharton';
        }
        $response = new RedirectResponse($referer_url);

        return $response;
    }
}
