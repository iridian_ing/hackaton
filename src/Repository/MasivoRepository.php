<?php

namespace App\Repository;

use App\Entity\Masivo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Masivo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Masivo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Masivo[]    findAll()
 * @method Masivo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasivoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Masivo::class);
    }

    // /**
    //  * @return Masivo[] Returns an array of Masivo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Masivo
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
