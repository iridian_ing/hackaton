<?php

namespace App\Repository;

use App\Entity\TipoReceptor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoReceptor|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoReceptor|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoReceptor[]    findAll()
 * @method TipoReceptor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoReceptorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoReceptor::class);
    }

    // /**
    //  * @return TipoReceptor[] Returns an array of TipoReceptor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoReceptor
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
