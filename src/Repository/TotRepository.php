<?php

namespace App\Repository;

use App\Entity\Tot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Tot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tot[]    findAll()
 * @method Tot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tot::class);
    }

    // /**
    //  * @return Tot[] Returns an array of Tot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tot
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
