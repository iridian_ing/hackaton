<?php

namespace App\Controller;

use App\Entity\PagoSuscripcion;
use App\Entity\PayuLog;
use App\Entity\Plan;
use App\Entity\Suscripcion;
use App\Service\QI;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class WebHookPayPalController extends AbstractController
{
    private  $qi;
    private $userManager;

    public function __construct(QI $qi, UserManagerInterface $userManager)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
    }

    /**
     * @Route("/webhook/paypal", name="webhook_paypal")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $log = new  PayuLog();
        $log->setData($request->getContent());
        $em->persist($log);
        $em->flush();
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new BadRequestHttpException('invalid json body: ' . json_last_error_msg());
        }
        $event_type = $data['event_type'];



        if($event_type == "BILLING.SUBSCRIPTION.ACTIVATED"){
            $email_address = $data['resource']['subscriber']['email_address'];
            $payer_id = $data['resource']['subscriber']['payer_id'];
            $billing_agreement_id = $data['resource']['id'];
            $plan_id = $data['resource']['plan_id'];
            $status = $data['resource']['status'];

            /* @var $suscripcion Suscripcion */
            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array("id_paypal"=>$billing_agreement_id));
            $suscripcion->setEmail($payer_id);
            $suscripcion->setPayerId($payer_id);
            $suscripcion->setPlanId($plan_id);
            $suscripcion->setStatus($status);
            $suscripcion->setActiva(true);
            $em->persist($suscripcion);
            $em->flush();

            $suscripcion->setEmail("");
            $suscripcion->setDuracion(1);
            $suscripcion->setIdPayu("");
            $suscripcion->setPayerId("");
            $suscripcion->setIdPaypal("");
            $suscripcion->setPlanId("");
            $suscripcion->setUsuario(null);
            $suscripcion->setValidaHasta(null);
            $this->qi->saveFire($suscripcion);

            return new Response('Hello PayPal', Response::HTTP_OK);
        }elseif ($event_type == "PAYMENT.SALE.COMPLETED"){
            $id = $data['resource']['id'];
            $state = $data['resource']['state'];
            $billing_agreement_id = $data['resource']['billing_agreement_id'];

            /* @var $suscripcion Suscripcion */
            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array("id_paypal"=>$billing_agreement_id));

            $pago = new PagoSuscripcion();
            $pago->setSuscripcion($suscripcion);
            $pago->setState($state);
            $pago->setIdPaypal($id);
            $em->persist($pago);
            $em->flush();

            $suscripcion->setActiva(true);
            $valida = new \DateTime();
            $valida = new \DateTime(date('Y-m-d', strtotime("+2 days",strtotime("+".$suscripcion->getDuracion()." months", $valida->getTimestamp()) ) ) );
            //dd($valida);
            $suscripcion->setValidaHasta($valida);

            $em->persist($suscripcion);
            $em->flush();

            $this->qi->saveFire($pago);
            $suscripcion->setEmail("");
            $suscripcion->setDuracion(0);
            $suscripcion->setIdPayu("");
            $suscripcion->setPayerId("");
            $suscripcion->setIdPaypal("");
            $suscripcion->setPlanId("");
            $suscripcion->setUsuario(null);
            $suscripcion->setValidaHasta(null);
            $this->qi->saveFire($suscripcion);

            return new Response('Hello PayPal', Response::HTTP_OK);
        }else if( $event_type == 'BILLING.SUBSCRIPTION.PAYMENT.FAILED'){
            $id = $data['resource']['id'];
            $state = $data['resource']['state'];
            $billing_agreement_id = $data['resource']['billing_agreement_id'];

            /* @var $suscripcion Suscripcion */
            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array("id_paypal"=>$billing_agreement_id));
            $pago = new PagoSuscripcion();
            $pago->setSuscripcion($suscripcion);
            $pago->setState('failed');
            $pago->setIdPaypal($id);
            $em->persist($pago);
            $em->flush();

            $suscripcion->setActiva(false);
            $em->persist($suscripcion);
            $em->flush();

            $this->qi->saveFire($pago);
            $suscripcion->setEmail("");
            $suscripcion->setDuracion(0);
            $suscripcion->setIdPayu("");
            $suscripcion->setPayerId("");
            $suscripcion->setIdPaypal("");
            $suscripcion->setPlanId("");
            $suscripcion->setUsuario(null);
            $suscripcion->setValidaHasta(null);
            $this->qi->saveFire($suscripcion);
        }
        else if( $event_type == 'BILLING.SUBSCRIPTION.EXPIRED'){
            $id = $data['id'];
            $state = $data['resource']['status'];
            $billing_agreement_id = $data['resource']['id'];

            /* @var $suscripcion Suscripcion */
            $suscripcion = $this->getDoctrine()->getRepository(Suscripcion::class)->findOneBy(array("id_paypal"=>$billing_agreement_id));
            $pago = new PagoSuscripcion();
            $pago->setSuscripcion($suscripcion);
            $pago->setState($state);
            $pago->setIdPaypal($id);
            $em->persist($pago);
            $em->flush();

            $suscripcion->setActiva(false);
            $em->persist($suscripcion);
            $em->flush();

            $this->qi->saveFire($pago);
            $suscripcion->setEmail("");
            $suscripcion->setDuracion(0);
            $suscripcion->setIdPayu("");
            $suscripcion->setPayerId("");
            $suscripcion->setIdPaypal("");
            $suscripcion->setPlanId("");
            $suscripcion->setUsuario(null);
            $suscripcion->setValidaHasta(null);
            $this->qi->saveFire($suscripcion);
        }
        //dump($event_type,$email_address, $payer_id, $billing_agreement_id, $plan_id, $status);
        return $this->render('web_hook_pay_pal/index.html.twig', [
            'controller_name' => 'WebHookPayPalController',
        ]);
    }


    /**
     * @Route("/webhook/suscripcionpaypal", name="crear_suscripcion_paypal")
     */
    public function crearSuscripcion(Request $request)
    {
        $plan = $this->getDoctrine()->getRepository(Plan::class)->find(1);
        $urlretorno = $this->generateUrl('esperando_activacion',array(),UrlGeneratorInterface::ABSOLUTE_URL);
        $data = $this->qi->crearSuscripcionPaypal($plan,$this->getUser(),$urlretorno);
        $id = $data->id;
        $link="";
        foreach ($data->links as $l){
            if($l->rel == 'approve'){
                $link = $l->href;
            }
        }

        $suscripcion = new Suscripcion();
        $suscripcion->setDuracion($plan->getDuracion());
        $valida = new \DateTime();
        $valida = new \DateTime(date('Y-m-d', strtotime("+2 days",strtotime("+".$suscripcion->getDuracion()." months", $valida->getTimestamp()) ) ) );
        //dd($valida);
        $suscripcion->setValidaHasta($valida);

        $suscripcion->setIdPaypal($id);
        $suscripcion->setUsuario($this->getUser());
        $suscripcion->setActiva(false);
        $suscripcion->setPlanSuscripcion($plan);
        $em = $this->getDoctrine()->getManager();
        $em->persist($suscripcion);
        $em->flush();

        return $this->redirect($link);

    }
    /**
     * @Route("/webhook/suscripcion/{id}", name="crear_suscripcion")
     */
    public function suscripcion(Request $request, $id)
    {
        $plan = $this->getDoctrine()->getRepository(Plan::class)->find(1);
        $suscripcion = new Suscripcion();
        $suscripcion->setDuracion(1);
        $valida = new \DateTime();
        $valida = new \DateTime(date('Y-m-d', strtotime("+2 days",strtotime("+".$suscripcion->getDuracion()." months", $valida->getTimestamp()) ) ) );
        //dd($valida);
        $suscripcion->setValidaHasta($valida);

        $suscripcion->setIdPaypal($id);
        $suscripcion->setUsuario($this->getUser());
        $suscripcion->setActiva(false);
        $suscripcion->setPlanSuscripcion($plan);
        $em = $this->getDoctrine()->getManager();
        $em->persist($suscripcion);
        $em->flush();

        $resultado = array("ok" => true);
        $response = new Response();
        $response->setContent(json_encode([
            'ok' => true,
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
