<?php

namespace App\Entity;

use App\Repository\JourneyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Galeria", inversedBy="images")
     */
    private $galeria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="imagenes")
     */
    private $producto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Servicio", inversedBy="galeria")
     */
    private $servicio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Categoria", mappedBy="imagen", cascade={"persist"})
     */
    private $categorias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="image")
     */
    private $productos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Journey", inversedBy="imagenes")
     */
    private $journey;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="image")
     */
    private $journeys;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Journey", mappedBy="imagfunda")
     */
    private $jourfunda;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion_fr;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
        $this->categorias = new ArrayCollection();
        $this->productos = new ArrayCollection();
        $this->journeys = new ArrayCollection();
        $this->jourfunda = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->image.' ';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage( $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitulo(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'titulo_'.$locale;
        return $this->{$field};
    }

    public function setTitulo(?string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'descripcion_'.$locale;
        return $this->{$field};
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getGaleria(): ?Galeria
    {
        return $this->galeria;
    }

    public function setGaleria(?Galeria $galeria): self
    {
        $this->galeria = $galeria;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }
       public function getJourney() : ?Journey
    {
        return $this->journey;
    }

    public function setJourney(Journey $journey): self
    {
        $this->journey = $journey;
        return $this;
    }


    public function getServicio(): ?Servicio
    {
        return $this->servicio;
    }

    public function setServicio(?Servicio $servicio): self
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * @return Collection|Categoria[]
     */
    public function getCategorias(): Collection
    {
        return $this->categorias;
    }

    public function addCategoria(Categoria $categoria): self
    {
        if (!$this->categorias->contains($categoria)) {
            $this->categorias[] = $categoria;
            $categoria->setImagen($this);
        }

        return $this;
    }

    public function removeCategoria(Categoria $categoria): self
    {
        if ($this->categorias->contains($categoria)) {
            $this->categorias->removeElement($categoria);
            // set the owning side to null (unless already changed)
            if ($categoria->getImagen() === $this) {
                $categoria->setImagen(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setImage($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getImage() === $this) {
                $producto->setImage(null);
            }
        }

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourneys(): Collection
    {
        return $this->journeys;
    }

    public function addJourney(Journey $journey): self
    {
        if (!$this->journeys->contains($journey)) {
            $this->journeys[] = $journey;
            $journey->setImage($this);
        }

        return $this;
    }

    public function removeJourney(Journey $journey): self
    {
        if ($this->journeys->contains($journey)) {
            $this->journeys->removeElement($journey);
            // set the owning side to null (unless already changed)
            if ($journey->getImage() === $this) {
                $journey->setImage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Journey[]
     */
    public function getJourfunda(): Collection
    {
        return $this->jourfunda;
    }

    public function addJourfunda(Journey $jourfunda): self
    {
        if (!$this->jourfunda->contains($jourfunda)) {
            $this->jourfunda[] = $jourfunda;
            $jourfunda->setImagfunda($this);
        }

        return $this;
    }

    public function removeJourfunda(Journey $jourfunda): self
    {
        if ($this->jourfunda->contains($jourfunda)) {
            $this->jourfunda->removeElement($jourfunda);
            // set the owning side to null (unless already changed)
            if ($jourfunda->getImagfunda() === $this) {
                $jourfunda->setImagfunda(null);
            }
        }

        return $this;
    }

    public function getTituloEs(): ?string
    {
        return $this->titulo_es;
    }

    public function setTituloEs(?string $titulo_es): self
    {
        $this->titulo_es = $titulo_es;

        return $this;
    }

    public function getTituloEn(): ?string
    {
        return $this->titulo_en;
    }

    public function setTituloEn(?string $titulo_en): self
    {
        $this->titulo_en = $titulo_en;

        return $this;
    }

    public function getTituloFr(): ?string
    {
        return $this->titulo_fr;
    }

    public function setTituloFr(?string $titulo_fr): self
    {
        $this->titulo_fr = $titulo_fr;

        return $this;
    }

    public function getDescripcionEs(): ?string
    {
        return $this->descripcion_es;
    }

    public function setDescripcionEs(?string $descripcion_es): self
    {
        $this->descripcion_es = $descripcion_es;

        return $this;
    }

    public function getDescripcionEn(): ?string
    {
        return $this->descripcion_en;
    }

    public function setDescripcionEn(?string $descripcion_en): self
    {
        $this->descripcion_en = $descripcion_en;

        return $this;
    }

    public function getDescripcionFr(): ?string
    {
        return $this->descripcion_fr;
    }

    public function setDescripcionFr(?string $descripcion_fr): self
    {
        $this->descripcion_fr = $descripcion_fr;

        return $this;
    }

    public function getRuta(){
        return '/uploads/images/products/'.$this->getImage();
    }
}
