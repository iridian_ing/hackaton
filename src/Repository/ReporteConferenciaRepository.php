<?php

namespace App\Repository;

use App\Entity\ReporteConferencia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReporteConferencia|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReporteConferencia|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReporteConferencia[]    findAll()
 * @method ReporteConferencia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReporteConferenciaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReporteConferencia::class);
    }

    // /**
    //  * @return ReporteConferencia[] Returns an array of ReporteConferencia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReporteConferencia
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
