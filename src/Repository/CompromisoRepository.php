<?php

namespace App\Repository;

use App\Entity\Compromiso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Compromiso|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compromiso|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compromiso[]    findAll()
 * @method Compromiso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompromisoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compromiso::class);
    }

    // /**
    //  * @return Compromiso[] Returns an array of Compromiso objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compromiso
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
