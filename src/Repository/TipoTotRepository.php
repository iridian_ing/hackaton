<?php

namespace App\Repository;

use App\Entity\TipoTot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TipoTot|null find($id, $lockMode = null, $lockVersion = null)
 * @method TipoTot|null findOneBy(array $criteria, array $orderBy = null)
 * @method TipoTot[]    findAll()
 * @method TipoTot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipoTotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TipoTot::class);
    }

    // /**
    //  * @return TipoTot[] Returns an array of TipoTot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TipoTot
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
