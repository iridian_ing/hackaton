<?php

namespace App\Repository;

use App\Entity\Razon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Razon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Razon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Razon[]    findAll()
 * @method Razon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RazonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Razon::class);
    }

    // /**
    //  * @return Razon[] Returns an array of Razon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Razon
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
