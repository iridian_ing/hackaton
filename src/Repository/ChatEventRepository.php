<?php

namespace App\Repository;

use App\Entity\ChatEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ChatEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChatEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChatEvent[]    findAll()
 * @method ChatEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChatEventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChatEvent::class);
    }

    // /**
    //  * @return ChatEvent[] Returns an array of ChatEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChatEvent
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
