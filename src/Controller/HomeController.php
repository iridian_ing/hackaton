<?php


namespace App\Controller;
use App\Entity\Contacto;

use App\Entity\Faq;
use App\Entity\PagoSuscripcion;
use App\Entity\Plan;
use App\Entity\Suscripcion;
use App\Entity\Usuario;
use App\Entity\Video;
use App\Form\ContactoType;
use App\Service\QI;
use FOS\UserBundle\Event\GetResponseNullableUserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class HomeController extends AbstractController
{
    private  $qi;
    private $userManager;
    private $retryTtl;
    private $tokenGenerator;


    public function __construct(QI $qi, UserManagerInterface $userManager,TokenGeneratorInterface $tokenGenerator)
    {
        $this->qi = $qi;
        $this->userManager = $userManager;
        $this->retryTtl = 7200;
        $this->tokenGenerator = $tokenGenerator;
    }



    /**
     * @Route("/resetting/iridian", name="reset-iridian")
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');

        $user = $this->userManager->findUserByUsernameOrEmail($username);

        $event = new GetResponseNullableUserEvent($user, $request);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        if (null !== $user && !$user->isPasswordRequestNonExpired($this->retryTtl)) {
            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->tokenGenerator->generateToken());
            }

            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            $this->sendResettingEmailMessage($user);

            $user->setPasswordRequestedAt(new \DateTime());
            $this->userManager->updateUser($user);

            $event = new GetResponseUserEvent($user, $request);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
        }

        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('username' => $username)));
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {

        $url = $this->generateUrl('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $mensaje= str_replace('%link%','<a href="'.$url.'">link</a>',$this->qi->getTextoBig('mail_contrasena'));
        $this->qi->sendMailIB($this->qi->getTexto('asunto_mail_resetting'),$user->getEmail(),$mensaje) ;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function home(Request $request)
    {

        $faqs = $this->getDoctrine()->getRepository(Faq::class)->findBy(array('visible'=>true),array('orden'=>'desc'));
        return $this->render('basic/home.html.twig', [
            'faqs'=>$faqs
        ]);
    }


    /**
     * @Route("/mail-prueba", name="mail-prueba")
     */
    public function mailPrueba(Request $request)
    {
        $asunto = 'probando';
        $to= 'ferney@iridian.co';
        $html = 'pruebasss';
        $r = $this->qi->sendMailIB($asunto,$to,$html);
        dd($r);

        return $this->render('basic/mail.html.twig', [
        ]);
    }


    /**
     * @Route("/contacto", name="contacto")
     */
    public function contacto(Request $request)
    {
        $contacto= new Contacto();
        $form = $this->createForm(ContactoType::class, $contacto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contacto);
            $entityManager->flush();
            $asunto = $this->qi->getSetting('asunto_mail_contacto');
            $receiver = $this->qi->getSetting('receiver_mail_contacto');
            $this->qi->sendMail($asunto,$receiver,$contacto->getHtml());
            return $this->render('contacto/contacto.html.twig', [
                'gracias'=>true
            ]);
        }
        return $this->render('contacto/contacto.html.twig', [
            'gracias'=>false,
            'form'=>$form->createView()
        ]);
    }




    /**
     * @Route("/terminos-condiciones", name="terminos")
     */
    public function terminos(Request $request)
    {
        $titulo=$this->qi->getTexto('title_terminos');
        $descripcion=$this->qi->getTextoBig('terminos');
        return $this->render('termino_politica/terminos.html.twig', ['titulo'=>$titulo,'descripcion'=>$descripcion,'ruta'=>'terminos', 'query'=>''
        ]);
    }

    /**
     * @Route("/politica-privacidad", name="politica")
     */
    public function politica(Request $request)
    {
        $titulo=$this->qi->getTexto('title_politica');
        $descripcion=$this->qi->getTextoBig('politica');
        return $this->render('termino_politica/terminos.html.twig', ['titulo'=>$titulo,'descripcion'=>$descripcion, 'ruta'=>'politica'
        ]);
    }



}
