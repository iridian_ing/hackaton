<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PagoSuscripcionRepository")
 */
class PagoSuscripcion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Suscripcion", inversedBy="pagos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $suscripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_paypal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_payu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $orderId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $monto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneda;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cus;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $reference_pol;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        if($this)
            return $this->getId().' ';
        return '';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSuscripcion(): ?Suscripcion
    {
        return $this->suscripcion;
    }

    public function setSuscripcion(?Suscripcion $suscripcion): self
    {
        $this->suscripcion = $suscripcion;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getIdPaypal(): ?string
    {
        return $this->id_paypal;
    }

    public function setIdPaypal(?string $id_paypal): self
    {
        $this->id_paypal = $id_paypal;

        return $this;
    }

    public function getIdPayu(): ?string
    {
        return $this->id_payu;
    }

    public function setIdPayu(?string $id_payu): self
    {
        $this->id_payu = $id_payu;

        return $this;
    }

    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    public function setOrderId(?string $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getMonto(): ?string
    {
        return $this->monto;
    }

    public function setMonto(?string $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(?string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getCus(): ?string
    {
        return $this->cus;
    }

    public function setCus(?string $cus): self
    {
        $this->cus = $cus;

        return $this;
    }

    public function getReferencePol(): ?string
    {
        return $this->reference_pol;
    }

    public function setReferencePol(?string $reference_pol): self
    {
        $this->reference_pol = $reference_pol;

        return $this;
    }
}
