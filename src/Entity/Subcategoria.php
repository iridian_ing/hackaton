<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubcategoriaRepository")
 */
class Subcategoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"persist"})
     */
    private $imagen;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Producto", mappedBy="subcategoria")
     */
    private $productos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_fr;


    public function __toString()
    {
        return $this->getNombre().' ';
    }

    public function __construct()
    {
        $this->productos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getNombre(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
         $field = 'nombre_'.$locale;

     return $this->{$field};
    }

   /* public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }*/

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }


    public function removeImagene(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getSubcategoria() === $this) {
                $producto->setSubcategoria(null);
            }
        }

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return Collection|Producto[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setSubcategoria($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            // set the owning side to null (unless already changed)
            if ($producto->getSubcategoria() === $this) {
                $producto->setSubcategoria(null);
            }
        }

        return $this;
    }

    public function getNombreEs(): ?string
    {
        return $this->nombre_es;
    }

    public function setNombreEs(?string $nombre_es): self
    {
        $this->nombre_es = $nombre_es;

        return $this;
    }

    public function getNombreEn(): ?string
    {
        return $this->nombre_en;
    }

    public function setNombreEn(?string $nombre_en): self
    {
        $this->nombre_en = $nombre_en;

        return $this;
    }

    public function getNombreFr(): ?string
    {
        return $this->nombre_fr;
    }

    public function setNombreFr(?string $nombre_fr): self
    {
        $this->nombre_fr = $nombre_fr;

        return $this;
    }


}
