<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_payu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $id_paypal;

    /**
     * @ORM\Column(type="integer")
     */
    private $duracion;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $es_recurrente;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function getIdPayu(): ?string
    {
        return $this->id_payu;
    }

    public function setIdPayu(?string $id_payu): self
    {
        $this->id_payu = $id_payu;

        return $this;
    }

    public function getIdPaypal(): ?string
    {
        return $this->id_paypal;
    }

    public function setIdPaypal(?string $id_paypal): self
    {
        $this->id_paypal = $id_paypal;

        return $this;
    }

    public function getDuracion(): ?int
    {
        return $this->duracion;
    }

    public function setDuracion(int $duracion): self
    {
        $this->duracion = $duracion;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getEsRecurrente(): ?bool
    {
        return $this->es_recurrente;
    }

    public function setEsRecurrente(bool $es_recurrente): self
    {
        $this->es_recurrente = $es_recurrente;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPrecioBase(){
        return $this->precio/(1+(19/100));
    }

    public function getImpuesto(){
        return $this->getPrecioBase()*(19/100);
    }

}
