<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JourneyRepository")
 */
class Journey
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orden;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="journey", cascade={"persist"})
     */
    private $imagenes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="journeys")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", inversedBy="journeys", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Producto", mappedBy="journeys")
     */
    private $productos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="ropa")
     * @ORM\JoinColumn(nullable=false)
     */
    private $libro;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="journeysropa")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ropa;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Producto", inversedBy="journeysgafa")
     * @ORM\JoinColumn(nullable=false)
     */
    private $gafas;

    /**
     * @ORM\Column(type="integer")
     */
    private $iva = 19;

     /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", inversedBy="jourfunda", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $imagfunda;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Favorito", mappedBy="journey")
     */
    private $favoritos;

     /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $desfundacion_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $desfundacion_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $desfundacion_fr;



    public function __construct()
    {
        $this->updatedAt = new \DateTime("now");
        $this->imagenes = new ArrayCollection();
        $this->productos = new ArrayCollection();
        $this->favoritos = new ArrayCollection();
    }


    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }


    public function setCategoria(Categoria $categoria): self
    {
        $this->categoria = $categoria;
        return $this;
    }


    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(?int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getDescuento(): ?int
    {
        return $this->descuento;
    }

    public function setDescuento(?int $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }


    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }


    /**
     * @return Collection|Image[]
     */
    public function getImagenes(): Collection
    {
        return $this->imagenes;
    }

    public function addImagene(Image $imagene): self
    {
        if (!$this->imagenes->contains($imagene)) {
            $this->imagenes[] = $imagene;
            $imagene->setJourney($this);
        }

        return $this;
    }

    public function removeImagene(Image $imagene): self
    {
        if ($this->imagenes->contains($imagene)) {
            $this->imagenes->removeElement($imagene);
            // set the owning side to null (unless already changed)
            if ($imagene->getJourney() === $this) {
                $imagene->setJourney(null);
            }
        }

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }


      /**
     * @return Collection|Producto[]
     */
        public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Producto $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->addJourney($this);
        }

        return $this;
    }

    public function removeProducto(Producto $producto): self
    {
        if ($this->productos->contains($producto)) {
            $this->productos->removeElement($producto);
            $producto->removeJourney($this);
        }

        return $this;
    }

        public function getLibro(): ?Producto
    {
        return $this->libro;
    }

    public function setLibro(?Producto $libro): self
    {
        $this->libro = $libro;

        return $this;
    }

    public function getRopa(): ?Producto
    {
        return $this->ropa;
    }

    public function setRopa(?Producto $ropa): self
    {
        $this->ropa = $ropa;

        return $this;
    }

    public function getGafas(): ?Producto
    {
        return $this->gafas;
    }

    public function setGafas(?Producto $gafas): self
    {
        $this->gafas = $gafas;

        return $this;
    }

    public function getIva(): ?int
    {
        return $this->iva;
    }

    public function setIva(int $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getResumen(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'resumen_'.$locale;
        return $this->{$field};
    }


    public function setResumen(?string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getDestacado(): ?bool
    {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self
    {
        $this->destacado = $destacado;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getDesfundacion(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'desfundacion_'.$locale;
        return $this->{$field};
    }

    public function setDesfundacion(?string $desfundacion): self
    {
        $this->desfundacion = $desfundacion;

        return $this;
    }

    public function getImagfunda(): ?Image
    {
        return $this->imagfunda;
    }

    public function setImagfunda(?Image $imagfunda): self
    {
        $this->imagfunda = $imagfunda;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return Collection|Favorito[]
     */
    public function getFavoritos(): Collection
    {
        return $this->favoritos;
    }

    public function addFavorito(Favorito $favorito): self
    {
        if (!$this->favoritos->contains($favorito)) {
            $this->favoritos[] = $favorito;
            $favorito->setJourney($this);
        }

        return $this;
    }

    public function removeFavorito(Favorito $favorito): self
    {
        if ($this->favoritos->contains($favorito)) {
            $this->favoritos->removeElement($favorito);
            // set the owning side to null (unless already changed)
            if ($favorito->getJourney() === $this) {
                $favorito->setJourney(null);
            }
        }

        return $this;
    }

    public function getResumenEs(): ?string
    {
        return $this->resumen_es;
    }

    public function setResumenEs(?string $resumen_es): self
    {
        $this->resumen_es = $resumen_es;

        return $this;
    }

    public function getResumenEn(): ?string
    {
        return $this->resumen_en;
    }

    public function setResumenEn(?string $resumen_en): self
    {
        $this->resumen_en = $resumen_en;

        return $this;
    }

    public function getResumenFr(): ?string
    {
        return $this->resumen_fr;
    }

    public function setResumenFr(?string $resumen_fr): self
    {
        $this->resumen_fr = $resumen_fr;

        return $this;
    }

    public function getDesfundacionEs(): ?string
    {
        return $this->desfundacion_es;
    }

    public function setDesfundacionEs(?string $desfundacion_es): self
    {
        $this->desfundacion_es = $desfundacion_es;

        return $this;
    }

    public function getDesfundacionEn(): ?string
    {
        return $this->desfundacion_en;
    }

    public function setDesfundacionEn(?string $desfundacion_en): self
    {
        $this->desfundacion_en = $desfundacion_en;

        return $this;
    }

    public function getDesfundacionFr(): ?string
    {
        return $this->desfundacion_fr;
    }

    public function setDesfundacionFr(?string $desfundacion_fr): self
    {
        $this->desfundacion_fr = $desfundacion_fr;

        return $this;
    }

}
