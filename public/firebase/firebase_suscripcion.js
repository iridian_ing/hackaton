$(function () {
    var id = $('#suscripcion').data('id');
    console.log(id);
    var ref = database.ref("Suscripcion/"+id).on("value", function(snapshot) {
        var changedSus = snapshot.val();
        var key = snapshot.key;
        //console.log("The updated post title is " + changedSus.activa, key);
        if(changedSus.activa){
            //animación de suscripción activada
            alertaJp();
            //window.location = '/es/videos';
        }
    });
    /*ref.orderByChild("suscripcion").equalTo(id).once("child_changes")
        .then(function(snapshot) {
            console.log(snapshot);
            console.log(snapshot.val());
            //animación de suscripción activada
            window.location = '/es/videos';
    });*/

    setTimeout(function(){
        //alertaJp();
    }, 4000);
});

function alertaJp() {
    if($('.alerta-Jp').length >= 1){
        $('.alerta-Jp').each(function () {
            var opcion = $(this).data('opcion');
            var texto = $(this).data('texto');
            console.log('datos', opcion, texto);
            $('body').append('<div class="alertaJp overlay"></div>'+
                '<div class="alertaJp contenedor">'+
                '<div class="w-alerta border-brown-3 text-center">'+
                    '<p>'+texto+'</p>'+
                    '<a class="bt-1 close" href="/es/videos">'+opcion+'</a>'+
                '</div>'+
            '</div>');
            setTimeout(function(){
                $('.alertaJp').addClass('view');
                $('.alertaJp .close').click(function () {
                    $('.alertaJp').removeClass('view');
                    setTimeout(function(){
                        $('.alertaJp').remove();
                    },1000);
                });
            }, 100);
        });
    }
}
