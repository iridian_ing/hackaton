<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriaBlog")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

     /**
     * @ORM\Column(type="date")
     */
    private $fecha;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"persist"})
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", cascade={"persist"})
     */
    private $imagen_grande;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubCategoriaBlog")
     */
    private $subcategoria;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulo_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $resumen_fr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenido_es;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenido_en;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenido_fr;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoria(): ?CategoriaBlog
    {
        return $this->categoria;
    }

    public function setCategoria(?CategoriaBlog $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getTitulo(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'titulo_'.$locale;
        return $this->{$field};
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getResumen(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'resumen_'.$locale;
        return $this->{$field};
    }

    public function setResumen(string $resumen): self
    {
        $this->resumen = $resumen;

        return $this;
    }

    public function getImagen(): ?Image
    {
        return $this->imagen;
    }

    public function setImagen(?Image $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getContenido(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'contenido_'.$locale;
        return $this->{$field};
    }

    public function setContenido(string $contenido): self
    {
        $this->contenido = $contenido;

        return $this;
    }

    public function getImagenGrande(): ?Image
    {
        return $this->imagen_grande;
    }

    public function setImagenGrande(?Image $imagen_grande): self
    {
        $this->imagen_grande = $imagen_grande;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getSubcategoria(): ?SubCategoriaBlog
    {
        return $this->subcategoria;
    }

    public function setSubcategoria(?SubCategoriaBlog $subcategoria): self
    {
        $this->subcategoria = $subcategoria;

        return $this;
    }

    public function getTituloEs(): ?string
    {
        return $this->titulo_es;
    }

    public function setTituloEs(?string $titulo_es): self
    {
        $this->titulo_es = $titulo_es;

        return $this;
    }

    public function getTituloEn(): ?string
    {
        return $this->titulo_en;
    }

    public function setTituloEn(?string $titulo_en): self
    {
        $this->titulo_en = $titulo_en;

        return $this;
    }

    public function getTituloFr(): ?string
    {
        return $this->titulo_fr;
    }

    public function setTituloFr(?string $titulo_fr): self
    {
        $this->titulo_fr = $titulo_fr;

        return $this;
    }

    public function getResumenEs(): ?string
    {
        return $this->resumen_es;
    }

    public function setResumenEs(?string $resumen_es): self
    {
        $this->resumen_es = $resumen_es;

        return $this;
    }

    public function getResumenEn(): ?string
    {
        return $this->resumen_en;
    }

    public function setResumenEn(?string $resumen_en): self
    {
        $this->resumen_en = $resumen_en;

        return $this;
    }

    public function getResumenFr(): ?string
    {
        return $this->resumen_fr;
    }

    public function setResumenFr(?string $resumen_fr): self
    {
        $this->resumen_fr = $resumen_fr;

        return $this;
    }

    public function getContenidoEs(): ?string
    {
        return $this->contenido_es;
    }

    public function setContenidoEs(?string $contenido_es): self
    {
        $this->contenido_es = $contenido_es;

        return $this;
    }

    public function getContenidoEn(): ?string
    {
        return $this->contenido_en;
    }

    public function setContenidoEn(?string $contenido_en): self
    {
        $this->contenido_en = $contenido_en;

        return $this;
    }

    public function getContenidoFr(): ?string
    {
        return $this->contenido_fr;
    }

    public function setContenidoFr(?string $contenido_fr): self
    {
        $this->contenido_fr = $contenido_fr;

        return $this;
    }
}
