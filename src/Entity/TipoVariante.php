<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TipoVarianteRepository")
 */
class TipoVariante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_es;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_en;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre_fr;

    public function __toString()
    {
        return $this->getNombre();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        $field = 'nombre_'.$locale;
        return $this->{$field};
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombreEs(): ?string
    {
        return $this->nombre_es;
    }

    public function setNombreEs(?string $nombre_es): self
    {
        $this->nombre_es = $nombre_es;

        return $this;
    }

    public function getNombreEn(): ?string
    {
        return $this->nombre_en;
    }

    public function setNombreEn(?string $nombre_en): self
    {
        $this->nombre_en = $nombre_en;

        return $this;
    }

    public function getNombreFr(): ?string
    {
        return $this->nombre_fr;
    }

    public function setNombreFr(?string $nombre_fr): self
    {
        $this->nombre_fr = $nombre_fr;

        return $this;
    }
}
