<?php
namespace App\EventSubscriber;

use App\Service\QI;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    protected $em;
    protected $qi;
    protected $rs;

    public function __construct(EntityManagerInterface $em, QI $qi, RequestStack $request_stack)
    {
        $this->em = $em;
        $this->qi = $qi;
        $this->rs = $request_stack;
    }

    public static function getSubscribedEvents()
    {
        return array(
            EasyAdminEvents::POST_UPDATE => array('onPostUpdate'),
            EasyAdminEvents::PRE_PERSIST => array('onPrePersist'),
            EasyAdminEvents::PRE_UPDATE=> array('onPreUpdate'),
            EasyAdminEvents::POST_PERSIST => array('onPostUpdate'),
            EasyAdminEvents::PRE_REMOVE => array('onPreRemove'),
            EasyAdminEvents::POST_REMOVE => array('onPostRemove'),
        );
    }
    function to_camel_case($str, $capitalise_first_char = false) {
        if($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/_([a-z])/', $func, $str);
    }

    public function onPostUpdate(GenericEvent $event)
    {
        $entity = $event->getSubject();
        //$this->qi->saveFire($entity);
    }

    public function onPostPersist(GenericEvent $event)
    {
        $entity = $event->getSubject();
        //$this->qi->saveFire($entity);
    }

    public function onPrePersist(GenericEvent $event)
    {
        $entity = $event->getSubject();

        /*$firekey = $entity->getId();
        $entity->setKeyFirebase($firekey);*/

        $event['entity'] = $entity;
    }

    public function onPreUpdate(GenericEvent $event)
    {
        $entity = $event->getSubject();
        /*
        if($entity->getKeyFirebase() == "") {
            $firekey = $entity->getId();
            $entity->setKeyFirebase($firekey);
        }
        */

        $event['entity'] = $entity;
    }

    public function onPreRemove(GenericEvent $event)
    {
        $entity = $event->getSubject();
        $session = $this->rs->getCurrentRequest()->getSession();
        $session->set('ent_id',$entity->getId());
        $session->set('ent_tabla',get_class($entity));
        //$this->qi->removeFire($entity);
        //dump($entity);
        //die();
    }

    public function onPostRemove(GenericEvent $event)
    {
        $entity = $event->getSubject();
        $session = $this->rs->getCurrentRequest()->getSession();
        $id = $session->get('ent_id');
        $tabla = $session->get('ent_tabla');
        $tabla = explode(':',$tabla);
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        //$this->qi->removeFireBack($tabla,$id);
        //dump($entity);
        //die();
    }
}
