<?php

namespace App\Repository;

use App\Entity\Apellido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Apellido|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apellido|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apellido[]    findAll()
 * @method Apellido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApellidoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Apellido::class);
    }

    // /**
    //  * @return Apellido[] Returns an array of Apellido objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Apellido
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
