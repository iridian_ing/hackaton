<?php

namespace App\Repository;

use App\Entity\Instalador;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Instalador|null find($id, $lockMode = null, $lockVersion = null)
 * @method Instalador|null findOneBy(array $criteria, array $orderBy = null)
 * @method Instalador[]    findAll()
 * @method Instalador[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstaladorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Instalador::class);
    }

    // /**
    //  * @return Instalador[] Returns an array of Instalador objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Instalador
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
